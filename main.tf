terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.13.1"
    }
  }
}

variable "port" {
  description= "port biach"
  default= 8080
  type= number
}
provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "terra" {
  metadata {
    name = "terra"
  }

}

resource "kubernetes_deployment" "terra_test" {
  metadata {
    name      = "terratest"
    namespace = kubernetes_namespace.terra.metadata.0.name
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "terra_app"
      }
    }
    template {
      metadata {
        labels = {
          app = "terra_app"
        }
      }
      spec {
        container {
          image = "nginx"
          name  = "nginx-cont"
          port {
            container_port = 80
          }
        }

        container {
          image = "alpine"
          name  = "alpine"
          port {
            container_port = 80
          }
        }

      }

    }

  }
}
resource "kubernetes_service" "terra_serv" {
  metadata {
    name      = "nginx"
    namespace = kubernetes_namespace.terra.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.terra_test.spec.0.template.0.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      name        = "http"
      port        = 80 
      target_port = 80

    }
  }

}

resource "kubernetes_ingress_v1" "terra_ing" {
  metadata {
    name      = "terra-ingress"
    namespace = kubernetes_namespace.terra.metadata.0.name
  }
  spec {
    rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.terra_serv.metadata.0.name
              port {
                number = kubernetes_service.terra_serv.spec.0.port.0.port
              }
            }
          }
          path = "/"
        }
      }
      host = "terra.nether.loc"
    }
  }

}
